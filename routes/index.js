var express = require('express');
var router = express.Router();
var mysql = require('mysql')
// Connected to database 
var conn = mysql.createConnection({
  host:'localhost',
  user:'root',
  password:'',
  database:'demo'
})
conn.connect((err)=>{
  if(err) throw err
  console.log("Connected!")
})

router.get('/',(req,res)=>{
  res.render('login')
})

// Main 
router.post('/login',(req,res)=>{
	const getEmail = req.body.email
	const getPassword = req.body.password
	const queryEmailandPassword =  'SELECT * FROM MST_Person_Information WHERE email = ? AND password = ?'
	conn.query(queryEmailandPassword,[getEmail,getPassword],(err,result)=>{
			if(err) return err
			if(result == ""){
				res.render('nomember')
			}else{
				if(result[0].status == "member"){
					res.redirect('/posts')
				}else {
					res.redirect('/management')
				}
			}
	})
})

// Management Route
router.get('/management',(req,res)=>{
	const queryInformation = 'SELECT id, name, age, email, status FROM MST_Person_Information WHERE 1'
	conn.query(queryInformation,(err,result)=>{
		if(err) throw err
		res.render('management',{infomations:result})
	})
})

router.get('/management/add',(req,res) => {
	res.render('addmember');
});

router.post('/management',(req,res)=>{
	var getSearch = req.body.search
	console.log(getSearch)
	var querySearching = 'SELECT id, name, age, email, status FROM MST_Person_Information WHERE ? IN (name,age,email,status)'
	var queryShowAll = 'SELECT id, name, age, email, status FROM MST_Person_Information WHERE 1'
	 if(getSearch == ""){
		conn.query(queryShowAll,(err,result)=>{
			if(err) throw err
			return res.render('management',{infomations:result})
		})
	}else{
		conn.query(querySearching,[getSearch],(err,result)=>{
			if(err) throw err
			return res.render('management',{infomations:result})
		})
	}
})

router.get('/management/edit/:id',(req,res) => {
	const edit_memberID = req.params.id;
	conn.query('SELECT * FROM MST_Person_Information WHERE id=?',[edit_memberID],(err,results) => {
		if(results){
			res.render('editmember',{
				infomation:results[0]
			});
		}
	});
});

router.get('/management/delete/:id',(req,res) => {
	conn.query('DELETE FROM MST_Person_Information WHERE id = ?', [req.params.id], (err, results) => {
			return res.redirect('/management');
	});	
});

router.post('/management/add',(req,res) => {
	const name = req.body.name;
	const age = Number(req.body.age);
	const email = req.body.email;
	const password = req.body.password;
	const status = req.body.status;
	const person = {
		name:name,
		age:age,
		email:email,
		password:password,
		status:status
	}
	conn.query('INSERT INTO MST_Person_Information SET ?',person,(err) => {
		if(err) throw err
		console.log('Data Inserted');
		return res.redirect('/management');
	});
});

router.get('/management/delete/:id',(req,res) => {
	conn.query('DELETE FROM MST_Person_Information WHERE id = ?', [req.params.id], (err, results) => {
			return res.redirect('/management');
	});	
});

// Posts Route
router.get('/posts',(req,res) => {
	conn.query('SELECT * FROM posts',(err,result) => {
		res.render('index',{
			posts:result
		});
	})
});

router.get('/post/add',(req,res) => {
	res.render('add');
});

router.post('/post/add',(req,res) => {
	const title = req.body.title;
	const content = req.body.content;
	const author_name = req.body.author_name;
	const post = {
		title:title,
		content:content,
		author:author_name,
		created_at:new Date()
	}
	conn.query('INSERT INTO posts SET ?',post,(err) => {
		console.log('Data Inserted');
		return res.redirect('/posts');
	});
});

router.get('/post/edit/:id',(req,res) => {
	
	const edit_postID = req.params.id;
	
	conn.query('SELECT * FROM posts WHERE id=?',[edit_postID],(err,results) => {
		if(results){
			res.render('edit',{
				post:results[0]
			});
		}
	});
});
router.post('/post/edit/:id',(req,res) => {
	const update_title = req.body.title;
	const update_content = req.body.content;
	const update_author_name = req.body.author_name;
	const userId = req.params.id;
	conn.query('UPDATE `posts` SET title = ?, content = ?, author = ? WHERE id = ?', [update_title, update_content, update_author_name, userId], (err, results) => {
        if(results.changedRows === 1){
            console.log('Post Updated');
        }
		return res.redirect('/posts');
    });
});

router.get('/post/delete/:id',(req,res) => {
    conn.query('DELETE FROM `posts` WHERE id = ?', [req.params.id], (err, results) => {
        return res.redirect('/posts');
    });	
});

module.exports = router;
